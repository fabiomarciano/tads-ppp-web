# TADS

Projeto web para integrar ao trabalho da disciplina "PROJETO PRÁTICO DE PROGRAMAÇÃO"

## Equipe

* `1716101030` ERASMO BORGES DA SILVA SOGABE


* `1716100119` FABIO ALESSANDRO MARCIANO
    * fabioamarciano@gmail.com
    * +55 11 979658102


* `2317100326` FABIO LUIZ DA SILVA
    * fabio_gump@uninove.edu.br
    * fabio_gump@hotmail.com (Skype)
    * +55 11 968833369


* `916117038` FÁBIO MACHADO BORBA


* `1716100694` GIRLANE SAMPAIO RAMOS
    * gisampaioramos@hotmail.com
    * +55 11 984862262


* `2216107822` JACKSON PEREIRA DOS SANTOS CRUZ
    * jackson.psc@bol.com.br
    * +55 11 967555911


* `2316100482` JARDEL DE JESUS BASTOS
    * jardeljbastos@gmail.com
    * +55 11 972412652

## Projeto

Criação do site do Salão de Cabeleireiros Largo Treze e suporte técnico ao mesmo.

O site deverá conter:

* Apresentação da empresa aos clientes, contando uma breve história do mesmo, bem como suas principais qualidades.
* Ter um campo de cadastro de clientes, com nome, endereço, CPF (primary key) e foto (opcional).
* Oferecer tabela de preços de produtos e serviços a clientes cadastrados.
* Oferecer possibilidade de pré agendamento de serviços para clientes cadastrados.
* Campo para elogios, reclamações e sugestões para clientes.

## Pré-requisitos

* Apache (http://apache.org/)
* PHP (http://php.net/)
* MySQL (https://www.mysql.com/)
* GIT (https://git-scm.com/)

## Instalação

### Windows

Para usuários windows utilize o xampp (https://www.apachefriends.org/index.html)

### Linux

sudo apt-get install apache2 php7.0 php7.0-cgi php7.0-cli php7.0-curl php7.0-gd php7.0-json php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-xmlrpc php7.0-xsl libapache2-mod-php7.0 mysql-server git

## Configuração MySQL

Configure o usuário para `root` e a senha para `admin`

Instale o script DB.sql

## Configuração Apache

Não é necessário configurar um vhost para rodar o projeto. Basta colocar o conteúdo da pasta htdocs do projeto no diretório web do apache de sua instalação.

## Rodando o Projeto

Clone o projeto para o diretório web instalado. Ex.:

`Linux`

cd ~

git clone git@bitbucket.org:fabiomarciano/tads-ppp-web.git tads-ppp-web

cp -pa ~/tads-ppp-web/htdocs/* /var/www/html/

Acesse seu diretório web:

Ex.: http://localhost/

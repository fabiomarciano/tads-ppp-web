<?php
require_once("includes/db.php");
require_once("includes/user.php");

$user = User::init();

?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Serviços</h2>
				</header>
				<article>
					<p>O Salão Largo Treze oferece os seguintes serviços:</p>

					<table>
						<caption>Tabela de preços</caption>
						<thead>
							<tr>
								<th>Serviço</th>
								<th>Preço<sup>1</sup></th>
							</tr>
						</thead>
						<tbody>
							<?php
									$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);
									$smtp = $DB->prepare("SELECT serviceName, servicePrice FROM SERVICE");
									$smtp->execute();
									$data = $smtp->fetchAll();
								?>
								<?php if (sizeof($data) > 0): ?>
								<?php foreach ($data as $item): ?>
								<tr>
									<th><?php print($item["serviceName"]); ?></th>
									<td><?php print($user->isOnline() ? "a partir de ".$item["servicePrice"] : "sob consulta"); ?></td>
								</tr>
								<?php endforeach; ?>	
								<?php endif; ?>
								<?php
									$smtp->closeCursor();
									$DB = null;
								?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="2">
									<?php if (!$user->isOnline()): ?>
										<p><sup>1</sup> Faça o login para ver os valores ou cadastre-se</p>
									<?php else: ?>
										<p><sup>1</sup> Valores em reais e sujeitos a alterações sem aviso prévio</p>
									<?php endif; ?>
								</td>
							</tr>
						</tfoot>
					</table>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>
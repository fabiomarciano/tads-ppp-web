<?php require_once("includes/user.php"); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Quem somos</h2>
				</header>
				<article>
					<p>Em 2001 a senhora Maria das Graças Soares iniciou as atividades no mercado de beleza do salão de cabeleireiro Largo Treze.</p>

					<p>Nos seus dezesseis anos de trajetória no ramo de beleza e estética, sempre trabalhou com produtos de qualidade e as melhores técnicas disponíveis no mercado e assim proporcionar aos seus clientes bem-estar.

					<p>Visando sempre a excelência no atendimento aos seus clientes, a equipe é composta por profissionais altamente qualificada que oferecem serviços personalizados.</p>

					<p>Reconhecido em todo o país por ter recebido o prêmio de melhor salão de beleza de 2016.</p>

					<p>O salão Largo Treze foi percursor em Dia da Noiva e Estética, com uma trajetória marcada pela elegância e o desenvolvimento da concepção “Beleza Completa”, onde em um único ambiente a beleza do corpo e o bem-estar da mente estão reunidos.</p>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>
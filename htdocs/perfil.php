<?php
require_once("includes/db.php");
require_once("includes/user.php");

$user = User::init();
if (!$user->isOnline()) {
	header("location: index.php");
}

$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);
$smtp = $DB->prepare("SELECT * FROM USER WHERE userCPF='".$user->userCPF."'");
$smtp->execute();

$data = $smtp->fetchAll();

if (sizeof($data) > 0) {
	foreach ($data[0] as $key => $value) {
		$user->$key = $value;
	}
}

$smtp->closeCursor();
$DB = null;

?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Perfil</h2>
				</header>
				<article>
					<div id="user-card">
						<?php if (@file_exists(__DIR__."/user/picture/".$user->userCPF.".jpg")): ?>
							<figure>
								<img src="<?php print("user/picture/".$user->userCPF.".jpg"); ?>">
							</figure>
						<?php endif; ?>
						<h3><?php print($user->userName); ?></h3>
						<code><?php print($user->userCPF); ?></code>
						<address><?php print($user->userEmail); ?></address>
					</div>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>
<?php require_once("includes/user.php"); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Bem-vindo ao Salão de Cabeleireiros Largo Treze</h2>
				</header>
				<article>
					<p>Este é o canal de relacionamento com o cliente do Salão de Cabeleiros Largo Treze.</p>

					<p>Através das áreas deste site, você poderá conhecer nossa história, se cadastrar, consultar a tabela de preços de nossos serviços, marcar seus horários e nos enviar sugestões, críticas, dúvidas e elogios.</p>

					<p>Algumas das funcionalidades do site exigem que você seja um usuário cadastrado, mas não se preocupe: o cadastro é rápido e gratuíto.</p>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>
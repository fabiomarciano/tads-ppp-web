<?php
	require_once("includes/db.php");
	require_once("includes/user.php");
	$user = User::init();
?>

<?php if ($user->isOnline() && isset($_SERVER["CONTENT_LENGTH"]) && sizeof($_SERVER["CONTENT_LENGTH"]) > 0): ?>

<?php

	$main = new stdClass();
	$main->status = isset($_POST) && sizeof($_POST) > 0;
	$main->log = array();
	$main->data = new stdClass();

	if ($main->status) {

		foreach ($_POST as $key => $value) {
			if (is_array($value)) {
				ksort($value);
				reset($value);
				if ($key == "scheduleServices") {
					$value = implode(" ", $value);
				} else {
					$value = implode("", $value);
				}
			} else {
				$value = trim($value);
			}

			$main->data->$key = $value;
		}

		if (!isset($main->data->scheduleDatetime) || $main->data->scheduleDatetime == "") {
			$main->status &= false;
			array_push($main->log, "O campo data é obrigatório");
		}

		if ($main->data->scheduleDatetime <= date("YmdHis")) {
			$main->status &= false;
			array_push($main->log, "O campo data não é uma data válida. Tente uma data no futuro.");
		}

		if (!isset($main->data->scheduleServices) || $main->data->scheduleServices == "") {
			$main->status &= false;
			array_push($main->log, "Escolha pelo menos um serviço para agendar");
		}

		if ($user->userCPF == "") {
			$main->status &= false;
			array_push($main->log, "É necessário ser um usuário cadastrado no sistema para agendar serviços pelo sistema");
		} else {
			$main->data->scheduleUser = $user->userCPF;
		}

		if ($main->status) {
			$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);

			$stmt = $DB->prepare("INSERT INTO SCHEDULE(scheduleDatetime, scheduleUser, scheduleServices) VALUES('".$main->data->scheduleDatetime."', '".$main->data->scheduleUser."', '".$main->data->scheduleServices."')");

			$main->status &= $stmt->execute();
			$stmt->closeCursor();

			if (!$main->status) {
				array_push($main->log, "Erro ao tentar marcar horário");
			}

			$DB = null;
		}

	} else {
		array_push($main->log, "Dados não enviados");
	}

?>

<?php endif; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Marcar Horário</h2>
				</header>
				<article>
					<?php if ($user->isOnline() && !isset($main)): ?>
					<p>Preencha o formulário abaixo para marcar o seu horário:</p>

					<form id="frm" action="#" method="post">
						<fieldset>
							<label for="userName">Data:</label>
							<small>dia:</small>
							<select name="scheduleDatetime[2]">
								<?php for ($i = 1; $i <= 31; $i++): ?>
									<?php $i = intval($i) <= 9 ? "0".intval($i) : intval($i); ?>
									<?php $c = $i != date("d") ? "" : " selected=\"selected\"" ?>
									<option value="<?php print($i); ?>"<?php print($c); ?>><?php print($i); ?></option>
								<?php endfor; ?>
							</select>
							<small>mês:</small>
							<select name="scheduleDatetime[1]">
								<?php for ($i = 1; $i <= 12; $i++): ?>
									<?php $i = intval($i) <= 9 ? "0".intval($i) : intval($i); ?>
									<?php $c = $i != date("m") ? "" : " selected=\"selected\"" ?>
									<option value="<?php print($i); ?>"<?php print($c); ?>><?php print($i); ?></option>
								<?php endfor; ?>
							</select>
							<small>ano:</small>
							<select name="scheduleDatetime[0]">
								<?php for ($i = date("Y"); $i <= date("Y") + 1; $i++): ?>
									<?php $c = $i != date("Y") ? "" : " selected=\"selected\"" ?>
									<option value="<?php print($i); ?>"<?php print($c); ?>><?php print($i); ?></option>
								<?php endfor; ?>
							</select>
						</fieldset>
						<fieldset>
							<label for="userName">Hora:</label>
							<small>Horas:</small>
							<select name="scheduleDatetime[3]">
								<?php for ($i = 8; $i <= 19; $i++): ?>
									<?php $i = intval($i) <= 9 ? "0".intval($i) : intval($i); ?>
									<?php $c = $i != date("H") ? "" : " selected=\"selected\"" ?>
									<option value="<?php print($i); ?>"<?php print($c); ?>><?php print($i); ?></option>
								<?php endfor; ?>
							</select>
							<small>Minutos:</small>
							<select name="scheduleDatetime[4]">
								<?php for ($i = 0; $i <= 50; $i += 10): ?>
									<?php $i = intval($i) <= 9 ? "0".intval($i) : intval($i); ?>
									<?php $c = $i > date("i") && $i < date("i") + 10 ? " selected=\"selected\"" : "" ?>
									<option value="<?php print($i); ?>"<?php print($c); ?>><?php print($i); ?></option>
								<?php endfor; ?>
							</select>
							<input type="hidden" name="scheduleDatetime[5]" value="00">
						</fieldset>
						<fieldset>
							<h3>Serviços:</h3>
							<p>Selecione todos os serviços que deseja realizar no horário:</p>
							<ul>
								<?php
									$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);
									$smtp = $DB->prepare("SELECT serviceID, serviceName FROM SERVICE");
									$smtp->execute();
									$data = $smtp->fetchAll();
								?>
								<?php if (sizeof($data) > 0): ?>
								<?php foreach ($data as $item): ?>
								<li>
									<label for="schedule-service-<?php print($item["serviceID"]); ?>"><?php print($item["serviceName"]); ?></label>
									<input type="checkbox" name="scheduleServices[]" id="schedule-service-<?php print($item["serviceID"]); ?>" value="<?php print($item["serviceID"]); ?>" />
								</li>	
								<?php endforeach; ?>	
								<?php endif; ?>
								<?php
									$smtp->closeCursor();
									$DB = null;
								?>
							</ul>
						</fieldset>
						<fieldset>
							<button type="submit">Agendar</button>
						</fieldset>
					</form>
					<?php endif; ?>

					<?php if ($user->isOnline() && isset($main)): ?>
						<?php if ($main->status): ?>
							<h3>Sucesso</h3>
							<p>O agendamento foi realizado com sucesso.</p>
							<p>Verifique todos os seus agendamentos na página de <a href="perfil.php">perfil</a>.</p>
						<?php else: ?>
							<h3>Erro</h3>
							<p>Ocorreram os seguintes erros durante o processo de agendamento:</p>
							<ol class="error-list">
								<?php if (sizeof($main->log) > 0): ?>
									<?php foreach ($main->log as $log): ?>
									<li><?php print($log); ?></li>
									<?php endforeach; ?>		
								<?php else: ?>
									<li>Erro ao tentar gravar dados. Pro favor tente novamente mais tarde.</li>
								<?php endif; ?>
							</ol>
							<p><a class="btn" href="marcar-horario.php">Tentar novamente</a></p>
						<?php endif; ?>		
					<?php endif; ?>

					<?php if (!$user->isOnline()): ?>
					<p>É necessário ser um usuário cadastrado no sistema para utilizar essa funcionalidade.</p>
					<?php endif; ?>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>

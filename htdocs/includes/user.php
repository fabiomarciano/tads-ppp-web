<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

session_start();

class User {
	private static $instance;
	private $data;

	private function __construct() {
		$this->data = new stdClass();
		if (isset($_SESSION["user"])) {
			$info = array();
			parse_str($_SESSION["user"], $info);

			if (sizeof($info) > 0) {
				foreach ($info as $key => $value) {
					$this->data->$key = $value;
				}
			}
		}
	}

	public static function init() {
		if (!isset(self::$instance)) {
			$self = __CLASS__;
			self::$instance = new $self;
		}
		return self::$instance;
	}

	public function __get($key) {
		return (isset($this->data->$key) ? $this->data->$key : null);
	}

	public function __set($key, $value) {
		$this->data->$key = $value;
	}

	public function isOnline() {
		return isset($this->data->userName) && !empty($this->data->userName) && isset($this->data->userCPF) && !empty($this->data->userCPF);
	}

	public function register() {
		if (isset($this->data->userName) && isset($this->data->userCPF)) {
			$_SESSION["user"] = "userName=".$this->data->userName."&userCPF=".$this->data->userCPF;
		}
	}

	public function logout() {
		if (isset($_SESSION["user"])) {
			$_SESSION["user"] = "";
			unset($_SESSION["user"]);
		}
	}
}

?>
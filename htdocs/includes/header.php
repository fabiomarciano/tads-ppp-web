<?php
require_once(__DIR__."/db.php");
require_once(__DIR__."/user.php");
$user = User::init();

?>
			<header>
				<div>
					<h1><a href="index.php">Salão de Cabeleireiros Largo Treze</a></h1>
					<?php if ($user->isOnline()): ?>
					<div id="wellcome">
						<address>Olá, <?php print($user->userName); ?></address>
						<ul>
							<li><a href="perfil.php">Perfil</a></li>
							<li><a href="logout.php">Sair</a></li>
						</ul>
					</div>
					<?php else: ?>
					<form action="login.php" method="post">
						<h5>Login</h5>
						<fieldset>
							<input type="text" name="userCPF" id="user-cpf" placeholder="CPF">
						</fieldset>
						<fieldset>
							<input type="password" name="userPassword" id="user-password" placeholder="Senha">
							<button type="submit">Entrar</button>
						</fieldset>
						<fieldset>
							<p><a href="cadastre-se.php">Cadastre-se</a></p>
						</fieldset>
					</form>
					<?php endif; ?>
				</div>
			</header>
			<nav>
				<ul>
					<li><a href="index.php">Página inicial</a></li>
					<li><a href="quem-somos.php">Quem somos</a></li>
					<li><a href="servicos.php">Serviços</a></li>
					<li><a href="marcar-horario.php">Marcar Horário</a></li>
					<li><a href="contato.php">Contato</a></li>
				</ul>
			</nav>
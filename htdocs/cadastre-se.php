<?php require_once("includes/db.php"); ?>
<?php require_once("includes/user.php"); ?>

<?php if (isset($_SERVER["CONTENT_LENGTH"]) && sizeof($_SERVER["CONTENT_LENGTH"]) > 0): ?>

<?php
	$main = new stdClass();
	$main->status = isset($_POST) && sizeof($_POST) > 0;
	$main->log = array();
	$main->data = new stdClass();

	if ($main->status) {
		foreach ($_POST as $key => $value) {
			$value = is_array($value) ? implode("", $value) : $value;
			if (!empty($value)) {
				$main->data->$key = trim($value);
			}
		}

		if (isset($main->data->userCPF)) {
			$main->data->userCPF = preg_replace("/[^0-9]+/", "", $main->data->userCPF);
			if (empty($main->data->userCPF)) {
				unset($main->data->userCPF);
			}
		}

		if (isset($main->data->userPassword)) {
			$main->data->userPassword = md5($main->data->userPassword);
			if (empty($main->data->userPassword)) {
				unset($main->data->userPassword);
			}
		}

		$required = array("userName" => "Nome do usuário obrigatório", "userCPF" => "O campo CPF é obrigatório", "userEmail" => "O e-mail é obrigatório", "userPassword" => "O campo password é obrigatório");

		foreach ($required as $key => $log) {
			if (!isset($main->data->$key)) {
				$main->status &= false;
				array_push($main->log, $log);
			}
		}

		if ($main->status) {
			$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);
			$stmt = $DB->prepare("SELECT count(*) AS userExists FROM USER WHERE userCPF = '".$main->data->userCPF."'");
			$stmt->execute();
			$data = $stmt->fetchAll();
			$data = is_array($data) ? $data[0] : $data;

			if (!intval($data["userExists"])) {

				$insert = $DB->prepare("INSERT INTO USER(userCPF, userName, userEmail, userPassword) VALUES('".$main->data->userCPF."', '".$main->data->userName."', '".$main->data->userEmail."', '".$main->data->userPassword."')");

				$main->status &= $insert->execute();
				$insert->closeCursor();

				if ($main->status) {
					if (isset($_FILES["userPicture"]) && sizeof($_FILES["userPicture"]) > 0) {
						$file = $_FILES["userPicture"];
						print_r($file);
						if (!$file["error"] && $file["type"] == "image/jpeg") {
							if (!@copy($file["tmp_name"], __DIR__."/user/picture/".$main->data->userCPF.".jpg")) {
								array_push($main->log, "Erro ao copiar imagem");
							}
						} else {
							array_push($main->log, "Imagem corrompida ou com formato inválido");
						}
					}
				} else {
					array_push($main->log, "Erro ao cadastrar usuário no DB");
				}

			} else {
				$main->status &= !intval($data["userExists"]);
				array_push($main->log, "Usuário já cadastrado com o CPF informado");
			}

			$stmt->closeCursor();
			$DB = null;
		}
	} else {
		echo "nenhum dado enviado";
	}

?>
<?php endif; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Cadastre-se</h2>
				</header>
				<article>
					<?php if (!isset($main)): ?>
					<p>Preencha o formulário abaixo para obter acesso ao sistema:</p>
					<?php else: ?>
						<?php if ($main->status): ?>
							<h3>Sucesso</h3>
						<?php else: ?>
							<h3>Erro</h3>
						<?php endif; ?>
					<?php endif; ?>
					<?php if (isset($main) && $main->status): ?>
						<p>Seu cadastro foi efetuado com sucesso!</p>
						<p>Agora você pode efetuar seu login e acessar as áreas reservadas à usuários cadastrados do site.</p>
					<?php endif; ?>
					<?php if (!isset($main) || (isset($main) && !$main->status)): ?>
					<?php if (isset($main) && !$main->status): ?>
						<p>Os seguintes erros foram encontrados em seu cadastro:</p>
						<ul>
							<?php foreach ($main->log as $log): ?>
								<li><?php print($log); ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<form id="frm" action="#" method="post" enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="8000000">
						<fieldset>
							<label for="user-name">Nome:</label>
							<input id="user-name" type="text" name="userName" value="<?php print(isset($main->data->userName) ? $main->data->userName : ""); ?>">
						</fieldset>
						<fieldset>
							<label for="user-cpf">CPF:</label>
							<input id="user-cpf" type="text" name="userCPF" value="<?php print(isset($main->data->userCPF) ? $main->data->userCPF : ""); ?>">
						</fieldset>
						<fieldset>
							<label for="user-mail">E-mail:</label>
							<input id="user-mail" type="text" name="userEmail" value="<?php print(isset($main->data->userEmail) ? $main->data->userEmail : ""); ?>">
						</fieldset>
						<fieldset>
							<label for="user-pswd">Senha:</label>
							<input id="user-pswd" type="password" name="userPassword">
						</fieldset>
						<fieldset>
							<label for="user-picture">Foto:</label>
							<input id="user-picture" type="file" name="userPicture">
							<small>Somente JPEGs de tamanho máximo 8mb</small>
						</fieldset>
						<fieldset>
							<button type="submit">Cadastrar</button>
						</fieldset>
					</form>
					<?php endif; ?>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>
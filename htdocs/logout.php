<?php

require_once(__DIR__."/includes/db.php");
require_once(__DIR__."/includes/user.php");

$user = User::init();
$user->logout();
header("location: index.php");

?>
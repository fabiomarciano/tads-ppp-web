<?php

require_once(__DIR__."/includes/db.php");
require_once(__DIR__."/includes/user.php");

$user = User::init();

$main = new stdClass();
$main->status = isset($_POST) && sizeof($_POST) > 0;
$main->log = array();
$main->data = new stdClass();

if ($main->status) {

	if (!$user->isOnline()) {
		foreach ($_POST as $key => $value) {
			$main->data->$key = trim($value);
		}

		$required = array("userCPF" => "O campo CPF é obrigatório", "userPassword" => "O campo senha é obrigatório");

		if (isset($main->data->userCPF)) {
			$main->data->userCPF = preg_replace("/[^0-9]+/", "", $main->data->userCPF);
		}

		foreach ($required as $key => $log) {
			if (!isset($main->data->$key) || empty($main->data->$key)) {
				$main->status &= false;
				array_push($main->log, $log);
			}
		}

		if ($main->status) {
			if (isset($main->data->userPassword)) {
				$main->data->userPassword = md5($main->data->userPassword);
			}

			$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);
			$stmt = $DB->prepare("SELECT userName, userCPF FROM USER WHERE userCPF = '".$main->data->userCPF."' AND userPassword = '".$main->data->userPassword."' LIMIT 1");

			$stmt->execute();
			$data = $stmt->fetchAll();

			if (is_array($data) && sizeof($data) > 0) {

				$data = is_array($data) ? $data[0] : $data;

				if (isset($data["userName"]) && isset($data["userCPF"])) {
					$user->userName = $data["userName"];
					$user->userCPF = $data["userCPF"];
					$user->register();
				}

			} else {
				$main->status &= false;
				array_push($main->log, "Usuário não encontrado ou senha incorreta");
			}

			$stmt->closeCursor();
			$DB = null;
		}

	} else {
		$main->status &= false;
		array_push($main->log, "Usuário já está online");
	}

} else {
	array_push($main->log, "Dados não enviados");
}

if ($main->status) {
	header("location: index.php");
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Erro</h2>
				</header>
				<article>
				<?php if (isset($main) && !$main->status): ?>
					<p>Os seguintes erros foram encontrados em seu cadastro:</p>
					<ul>
						<?php foreach ($main->log as $log): ?>
							<li><?php print($log); ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>
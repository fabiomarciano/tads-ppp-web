<?php
	require_once("includes/db.php");
	require_once("includes/user.php");
	$user = User::init();
?>

<?php if (isset($_SERVER["CONTENT_LENGTH"]) && sizeof($_SERVER["CONTENT_LENGTH"]) > 0): ?>

<?php

	$main = new stdClass();
	$main->status = isset($_POST) && sizeof($_POST) > 0;
	$main->log = array();
	$main->data = new stdClass();

	if ($main->status) {

		foreach ($_POST as $key => $value) {
			$main->data->$key = trim($value);
		}

		$required = array("contactUser" => "Campo nome é obrigatório", "contactType" => "O campo tipo é obrigatório", "contactMessage" => "O campo mensagem é obrigatório");

		foreach ($required as $key => $log) {
			if (!isset($main->data->$key) || (isset($main->data->$key) && $main->data->$key == "")) {
				$main->status &= false;
				array_push($main->log, $log);
			}
		}

		if ($main->status) {
			$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);

			$stmt = $DB->prepare("INSERT INTO CONTACT(contactUser, contactType, contactMessage) VALUES('".$main->data->contactUser."', '".$main->data->contactType."', '".$main->data->contactMessage."')");

			$main->status &= $stmt->execute();
			$stmt->closeCursor();

			if (!$main->status) {
				array_push($main->log, "Erro ao tentar enviar mensagem");
			}

			$DB = null;
		}

	} else {
		array_push($main->log, "Dados não enviados");
	}
?>

<?php endif; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />
		<title></title>
		<link rel="stylesheet" type="text/css" href="assets/style/main.css">
		<script type="text/javascript" src="assets/script/main.js"></script>
	</head>
	<body>
		<main>
			<?php require_once("includes/header.php"); ?>
			<section>
				<header>
					<h2>Contato</h2>
				</header>
				<article>
					<?php if (isset($main) && $main->status): ?>
						<h3>Obrigado!</h3>
						<p>Sua mensagem foi enviada com sucesso.</p>
						<p>Sua opinião é muito importante para o nosso crescimento e melhoria de nossos serviços.</p>
					<?php endif;?>
					<?php if (isset($main) && !$main->status): ?>
						<h3>Erro</h3>
						<p>Sua mensagem não pode ser enviada porque ocorreram os seguintes erros:</p>
						<ol class="error-list">
							<?php if (sizeof($main->log) > 0): ?>
								<?php foreach ($main->log as $log): ?>
									<li><?php print($log); ?></li>
								<?php endforeach; ?>		
							<?php else: ?>
								<li>Erro ao tentar gravar no banco de dados</li>
							<?php endif; ?>
						</ol>
						<p>Por favor tente novamente.</p>
					<?php endif;?>
					<?php if (!isset($main) || (isset($main) && !$main->status)): ?>
					<p>Preencha o formulário abaixo para nos enviar suas críticas, dúvidas, sugestões e elogios:</p>
					<form id="frm" action="#" method="post">
						<fieldset>
							<label for="contact-type">Tipo:</label>
							<select id="contact-type" name="contactType">
							<?php
								$DB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PSWD);
								$smtp = $DB->prepare("SELECT contactTypeID, contactTypeLabel FROM CONTACT_TYPE");
								$smtp->execute();
								$data = $smtp->fetchAll();
							?>
							<?php if (sizeof($data) > 0): ?>
							<option value="">Selecione</option>
							<?php foreach ($data as $item): ?>
							<?php $c = $item["contactTypeID"] == $main->data->contactType ? " selected=\"selected\"" : ""; ?>
							<option value="<?php print($item["contactTypeID"]); ?>"<?php print($c); ?>><?php print($item["contactTypeLabel"]); ?></option>
							<?php endforeach; ?>	
							<?php endif; ?>
							<?php
								$smtp->closeCursor();
								$DB = null;
							?>
							</select>
						</fieldset>
						<fieldset>
							<label for="contact-user">Nome:</label>
							<input id="contact-user" type="text" name="contactUser" value="<?php print(isset($main->data->contactUser) ? $main->data->contactUser : ($user->userName != "" ? $user->userName : "")); ?>">
						</fieldset>
						<fieldset>
							<label for="contact-message">Mensagem:</label>
							<textarea rows="10" cols="10" id="contact-message" name="contactMessage" placeholder="Digite sua mensagem"></textarea>
						</fieldset>
						<fieldset>
							<button type="submit">Enviar</button>
						</fieldset>
					</form>
					<?php endif; ?>
				</article>
			</section>
			<?php require_once("includes/footer.php"); ?>
		</main>
	</body>
</html>